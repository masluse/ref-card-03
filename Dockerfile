FROM maven:3.8.5-openjdk-17-slim as builder
COPY src /src
COPY pom.xml /
RUN mvn -f pom.xml clean package
FROM eclipse-temurin:17-jre-alpine
COPY --from=builder /target/*.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
